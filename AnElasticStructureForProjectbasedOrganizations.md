
An Elastic Structure for Project-based Organizations
===================================================================================================================

# Content
[toc]

# Version
  Version | Author   | Date        | Changes                   
  ------- | -------- | ----------  | ------------------------- 
  0.1     | Jin Feng | 2019.07.14  | Initial Draft             
  0.2     | Jin Feng | 2020.01.19  | Update structural diagram
  0.9     | Jin Feng | 2020.01.27  | Complete the template, Project's lifecycle, and sort out the whole
  1.0     | Jin Feng | 2020.01.28  | Add the physical containers


# Overview
We are facing lots of challenges from developing products and projects. Per Convey's law the organization's communication structure dictates the software design, so here we are trying to tackle challenges with an elastic organizational structure. In this article we want to propose an elastic organizational structure for project-based companies to adapt varieties of products, and to maximize the resources usages.

Before going on, I would like to give you the definitions of product, project, and project-based organizations used in this article.

  * A product is the piece of invisible software or the combination of mechanical and hardware with embedded software. 
  * A project is a managed intellectual activity to develop a product.
  * “Project-based organizations revolve around the concept that a group of individual or firms join together with the explicit purpose of producing a tangible set of outputs that can be physical (e.g. a building), logical (e.g. software code) or social (e.g. a marketing or public relations campaign)” [21]. 

# Motives
In this chapter I will give you situations we are facing and the reasons why we need an more effective structure.

1.	The requirements, designs, and implementations could be change
2.	Lots of and kinds of projects could proceed simultaneously. These projects could be mass productions, demos, POC, pre-development, internal, external, only applications, whole platform, e.t.c
3.	Varieties of products emerge and disappear rapidly
4.	Some products or projects cover hardware, bsp, framework, application and backend. Some might be only software components.
5.	If the number of products is no more than two (such as only IVN and VX2), it is too narrow
6.	It is hard to recruit qualified software engineers on demand unlike the production workers, but the employees' utilization rate has peak and trough.
7.	Some employees in some projects are busy, the others might be leisure.
8.	The skills, knowledge, and some software components are hardly share among different of  products (such as TBOX/V2X and IVI). Actually they have lots of commons from perstectives of software engineering.
9.	We are lack of all-rounders and specialists. Developers are valuables assets for the company. Since it is hard to employ many technical aces to be worth a ten. Division of labor on the basis of specialization is better choice.  Assigning specific functions to a junior one, and let he or she be an expert in this specialized area. Fusing these ordinary engineers who is only expert in some modules can let engineering department be an unified expert from the view of external side.
10.	High staff turnover
11.	The software components are re-developed again and again in different projects of same product
12.	The software components are re-developed again and again for different products
13.	Too many communications between internals and externals
14.	Puss the buck
15.	Response is not quick
16.	The work scope is not clear. If let it be, some modules will be dragged, communication cost will increase, dispute will occur, chaos will follow, and project will be delayed. Europe has ”The Empire was killed by iron nails”, and we have "A small leak will sink a great ship". So it is really necessary and critical to have a clear and definitive technical work scope and module assignments. 

# Objectives
Here we list the properties the desired structures should have.  

1.	Flexible for design changes[1] and varieties of products
2.	Scalable for different company sizes
3.	Can cover varieties of projects/products occurred in parallel 
4.	Decouple the organizational structure from products and business
5.	Every component or a combination of several components can be product and profitable
6.	The resources especial human resources shall be shared
7.	The skills and knowledge shall be shared among kinds of products
8.	The division of work shall be clear-cut, each one being charged with specific responsibilities.
9.	The talents pool shall be big enough and like a spindle to resist the high turnover
10.	The company's growth is driven by the ability of R&D rather than marketing/sales
11.	The common software components shall be abstracted, reused and evolved for kinds of projects and products.
12.	A project manager is necessary whatever the project it is
These attributes will be against the problems listed in motives chapter

# Theories
Before diving into the detailes of organizational structures, let us review some laws, rules, principles, and software hierarchy which will guide the design of this organizational structure.

## Microsoft Research
### Content
> "Organizational metrics can predict software failure-proneness with a precision and recall of 85%"
> <p align="right"> -- Nachiappan Nagappan, Microsoft Research (2009)<sup>[22]</sup> </p>

Often software systems are developed by organizations consisting of many teams of individuals working together. Brooks stats in the ***Mythical Man Month*** book that product quality is strongly affected by organization structure. Unfortunately there has been little empirical evidence to date to substantiate this assertion. The results of ***The Influence of Organizational Structure on Software Quality: An Empirical Case Study*** provide empirical evidence that the organizational metrics are related to, and are effective predictors of failure-proneness.

### Conclusion
* Product quality is strongly affected by organization structure.

## Dunbar's Number
### Content
> A measurement of the "cognitive limit to the number of individuals with whom any one person can maintain stable relationships."
>
> * Intimate Friends: 5
> * Trusted Friends: 15
> * Close Friends: 35
> * Casual Friends: 150
> <p align="right"> -- Robin Dunbar, 1992 </p>

### Conclusion
Limit your team members

## Divide-and-Conquer
### Content
> Break problem into several suproblems that are similar to the original problem but smaller in size, solve the subproblems recursively, and then combine these solution to create a solution to the roginal problem.
> <p align="right"> -- Computer Algorithm </p>

In divide-and-conquer,  we solve a problem recursively, applying three steps at each level of the recursion:  
**Divide** the problem into a number of subproblems that are smaller instances of the same problem.  
**Conquer** the subproblems by solving them recursively. If the subproblem sizes are small enough, however, just solve the subproblems in a straightforward manner.  
**Combine** the solutions to the subproblems into the solution for the original problem

When the subproblems are large enough to solve recursively, we call that the recursive case .  Once the subproblems become small enough that we no longer recurse, we say that the recursion “bottoms out” and that we have gotten down to the base case . Sometimes, in addition to subproblems that are smaller instances of the same problem, we have to solve subproblems that are not quite the same as the original
problem. We consider solving such subproblems as part of the combine step <sup>[17]</sup>.

### Conclusion
* Break big organization into divisions
* Break big project into packages
* Break big product into components
* The architects of Organization, project, and product are necessary, who are responsible for breaking down

## SNAFU
### Content
> "Situation Normal, All Fucked Up"
> <p align="right">-- A Military Acronym, Word War Two</p>

The SNAFU Principle is a sociological notion popular among Discordians. In its simplest form, it states that communication is only possible between equals. In a hierarchy, people inevitably distort the truth when dealing with their superiors, in order to curry favor or escape punishment. As a consequence, said superiors operate from an increasingly distorted view of the world, resulting in bad decisions. SNAFU itself is a military acronym for "Situation Normal, All Fucked Up". This is sometimes sanitized for innocent civilians as "situation normal, all fouled up"<sup>[11]</sup>.

### Conclusion
* This principle indicates that a flat organizational chart is better. Keep the organization hierarchical layers as less as possible. It is better no more than 3.
* Direct communication is preferable

## Conway's Law
The Melvin E. Conway's article ***How Do Commitees Invent?*** is valuable for every architect even if it was published in 1968, half century ago.

### Conway's (First) Law
#### Communication Dictates Design

> Any organization that designs a system (defined more broadly here than just information systems) will inevitably produce a design whose structure is a copy of the organization's communication structure.
> <p align="right">--Mel Conway, 1967</p>

#### Conclusion
* Because the design that occurs first is almost never the best possible, the prevailing system concept may need to change. Therefore, flexibility of organization is important to effective design<sup>[1]</sup>.
* Conway’s (first) Law tells us team size is important so...Make the teams as small as necessary. 
  * *Assesment:* If you don’t have a personal relationship with every member of your team, your team is probably too big. 
  * *Guidance:* Aim for team size of “Dunbar level 1” (5), possibly “Dunbar level 2” (15).
* Actively manage communications within the teams and across teams
* Reduce effort required to locate and interact with the "right people"
* Increase communications
  * Real-time chat tools
  * Video Conferencing
  * Online forums/news groups
  * Wiki and web sites  
  
### Second Law
#### Doing it Over
> "There is never enough time to do something right, but there is always enough time to do it over."
> <p align="right">-- Mel Conway, 1967</p>

#### Conclusion
  * Conway’s Second Law tells us problem size is important so...Make the solution as small as necessary.
    * *Assessment:* If you (or your team) cannot explain all the code in your release package, your release is too large
    * *Guidance:* Execute many small releases instead of a few large releases.
  * Support continuous process
    * Implement small changes
    * Test immediately
    * Deploy constantly
  * Shorten the feedback loop as much as possbile 

### Third Law
#### Homomorphism
> There is a homomorphism from the linear graph of a system to the linear graph of its design organization.
> <p align="right">--Mel Conway, 1967</p>

Here homomorphism is a transformation of one set into another that preserves in the second set the relations between elements of the first.

![](./homomorphism.png)

So this is another study from Microsoft, large scale software teams and how they work on interactions.

![](./coordinate.png)
**Figure**: How engineers mitigate (anticipated) problems with dependencies on other teams.

#### Conclusion
* Conway’s Third Law tells us cross-team independence is important. So...Make each team fully independent. If you have to hold a release until some other team is ready, you are not an independent team <sup>[15]</sup>.
* Organize your teams in order to achieve desired system.
* Organize teams by product or BU
  * Combine design, develop, test, and deploy
  * Include storage, business process, and UI
  * Allow team autonomy within their boundary
  * Require team to inter-operate, not integrate

### Forth Law
#### Deintegration
> The structures of large systems tend to disintegrate during development, qualitatively more so than with small systems.
> <p align="right">--Mel Conway, 1967</p>

Three reasons disintegration occurs.  
  **Reason #1** The realization that the system will be large, together with organization pressures, make irresistible the temptation to assign too many people to a design effort.  
  **Reason #2** Application of the conventional wisdom of management to a large design organization causes its communication structure to disintegrate.  
  **Reason #3** Homomorphism insures that the structure of the system will reflect the disintegration which has occurred in the design organization.

The Standish Group Chaos Report<sup>[18]</sup> confirms the Conway's forth law. C1 to C5 complexity across the top, size from S1 to S5 down the bottom, you can see once you get into high complexity high size the failure rate is astronomical, the opportunity for failure just increases exponentially. 
![](./sizeComplexity.jpg)

#### Conclusion
* Time is against large teams. So...Make release cycles short and small.
  * *Assesment:* If your release dates are often missed, your scope is too big. 
  * *Guidance:* Aim for a scope that supports a release cycle of two weeks or less
* Make the team as small as necessary
  * Resist urge to grow teams in response to deadlines
  * Consider Dunbar's group when sizing team
  * Be prepared to break into smaller teams
* Make the system as small as necessary
  * Be parepared to break into smaller systems for smaller teams
  * Architects are necessary to achive the conceptual integirty while break down the system.

## Brooks' Law
### Content
> Adding manpower to a late software project makes it latter  
> 
> Intercommunication formula: n(n-1)/2  
> 5*(5-1)/2     = 10  
> 15*(15-1)/2   = 105  
> 50*(50-1)/2   = 1,225  
> 150(150-1)/2  = 11,175    
> <p align="right">-- Fred Books, 1975</p>

Since software construction is inherently a systems effort --- an exercise in complex interrelationships---communication effort is great, and it quickly dominates the decrease in individual task time brought about by partitioning. Adding more men then lengthens, not shortens, the schedule.

### Conclusion
The number of months of a project depends upon its sequential constraints. The maximum number of men depends upon the number of independent subtasks. From these two quantities one can derive schedules using fewer men and more months.

# Template
Based on the laws above, we would like to propose an elastic organizational structure for general companies, especially software-based.

## Set
As we know the organizational structure is an human group actually. Some rules from sociology can guide our design:

* The R&D hierarchical layers should be no more than 3<sup>[11]</sup> ← SNAFU
* The direct subordinates should be no more than 9 members<sup>[9]</sup> ← Dunbar's Number
* Divide and conquer
  
With the limitations of above rules, here we show the general hierarchical layers and their descriptions. In theory the largest size we can support in one department is 10x10x10=1000 persons.

![](./hierachy.png)

Here we call *Hi, Hii, and Hii* a set. The properties of a Set:

* A set is your small family of people having ‘similar skills’ and working within the same general ‘competency area’. There are sets which are also known as the specialists
* Sets can be nested, and a compound set could contain other sets. The nesting level shall be as less as possible, no more than 4.
* The purposes of group are skills and knowledge sharing, routine management, artifacts evolving and reusing.
* The number of members in a set shall be less than 15

The Ingredients of a set:  

* Set Lead  
  * is also a line manager of the set members and support them in their personal growth and specific challenges.
  * Set lead itself is a set member too
  * Represent a set
* Members
  * A member can be an individual person or a set which is represented by its set lead

## Task
A task is an input to a set, which has requirements and expected output. 

* A task can be from set internal and external.  
* All set members, or several could process a task.
Now the question is that how to self-coordinate set’s members to process task.

## Process Task
We use “divide and conquer” and virtual team methodologies to process a task generally.
A virtual team consists of all the involved members from different sets to complete the task.

![](./processTask.png)

A virtual team consists of all the involved members from different sets to complete the task.

## Hierachy

### Group(Hiii)
***Description***

A combat unit which is focus on one or several similar  tight coupled functions.

***Memebers***

* GL: Management, planning, interface, a little coding
* TL: Internal architecture, code review, core coding, bug fix, COC
* Developers: coding, bug fix, testing

***Comments***

GL and TL can be one person Developers shall less than 10 persons

### Devision(Hii)

***Description***

Combine several groups for easy management

***Memebers***

* Manager: Management, coordination,
* GLs

***Comments***
TLs shall less than 10 persons

### Department(Hi)

***Description***

Combine several Divisions for easy management

***Memebers***

Director: Management, coordination, direction
Managers:

***Comments***

Managers shall be less than 10 persons

### Groups
The essence of this organizational structure is modular which is represented with form of group which is botom-down set. A group:  

* is an aggregate of individuals who are cohesive works
* can be a functional  (such as BT group), or technical unit (BSP group), or management unit (such as PM group). 
* is a combat team
* could be a profit unit
* is spindle shape to resist turnover
* is a place to deposit  knowledge and skills of both technique and management.
* is an area to evolve reusable components for kinds of products and to implement COC (center of competence)

To setup so highly efficient group you can refer to the chapter 3 "A Surgical Team" of the book called "The Mythical Man-Month".  The surgeon shall be backed up for leaving.

Since we are creating a general organizational structure for all kinds of compnayies, *IVI Tire One* which covers mechanical, hardware, full software stacks(bsp, framework, applications, backend), and manufacturing is one of them. We need to utilize the advanteges of different kinds of groups which are functional groups, cross-functional groups, and fpecial forced virtual groups.

#### Functional Groups

TODO: Such as bsp group which is good for knowledge sharing.

#### Cross-functional Groups<sup>[20]</sup>

TODO: (such as audio team which crosses requirements, architects, project management, testing, dsp, bsp, framework, and applications)

#### Virtual Groups
TODO: These kinds of groups are only exist during projects for special forces.

## Communication
A strand of these scholars is particularly interested in the relation between product and organizational modularity identifying the following relation: “Integral products should be developed by integral organizations (tightly connected organizational units to maximize ease of communication and minimize the risk of opportunism). Modular products should be developed by autonomous, loosely coupled, easily reconfigurable organizations. Indeed, the adoption of standards reduces the level of asset specificity (Argyres,1999) and, in turn, the need to exercise managerial authority. Product modularity also reduces the need for communication due to information hiding, whereby knowledge about the ‘interior’ of each module does not need to be shared.”<sup>[21]</sup>.

### Typical Ways of Communication

#### Hierachical Organization

TODO: Communicate vertically

#### Netowrk Organization

TODO: Communicate 

#### Connected Organization
The connected organization is the blend of hierachical and network structures.
![](./connectedOrganization.png)

### Conclusion
TODO: direct communication between groups ??

## Process
* Take each output as a product which is developed via project which is managed by project manager.
* SAFe<sup>[25]</sup>

# Instance
In Last chapter we describe an general elastic template of creating an organizational structure. Now An concrete example for the companies which produce in-vehicle infortainments, instrument clusters, and other products with both hardware and software instatiated from template will be given. Before unveiling, let us review the static structure of product itself and the life cycle of a project to develop a product.

## The Ingredients of Product
Please refer to the article *"The Ingredients of Product<sup>[27]</sup>"*.

## The Project's Lifecycle
![](./projectactivityprocess.png)

TODO: describe the project's lifecycle

## Organizational Structure
![](./organizationalstructure.png)

### The Roles' Responsibilities
TODO: Sales, Product, Engineering, Production, Project Manager (PM), System, System Test, Software Requirements, Software Architecture, Software Integration, Software Test,Development...

### Create New Groups
The most groups in organizational structure are gathered based on layers and functions, such as bsp, framework, and application. However some critical functions such as audio are sophisticated, cross-layered, and issue-prone, it is necessary to have a temporal virtual cross-layered group for a specific project or a permanent one. Let's take audio as an example to show how to build up a new group.

1. Firstly the audio functions and its developments scatter among bsp, framework, and application. We call application.audio, framework.audio, and bsp.audio respectively.
2. Extract the audio parts from layer level to form audio group called dev.audio in development level. Now all the development works related to audio shall be covevered by audio group.
3. If the dev.audio is not enough, we continue extract the audio parts of architecture, requirements, software project management, software testing, and integration to form software level audio called software.audio. Now all the software activites related to audio shall be done by software.audio.
4. The steps can be continued to form engineering.audio and org.audio if it is necessary

## Process
We take every outputs of activities such as POC, demo, pre-development as product whatever the maturity degree.   

* Each product shall be managed by project manager.
* A product is produced via assembling the outputs of groups.
* A project is the collaboration among groups coordinated by project manager.

The SAFe process can be used to develop product.

## Products
In theory we can take the output of a group or combination of outputs from several groups as a product. Thus theoratically the number of products is 2^n ( 2^n =C0n+C1n +...+Cnn)  
Based on this thinking,  we can get the conclusions:  
•	The products list can extend  intensively for our sales people.   
•	The employees' utilization rate trough can be compensated by massive products

Now we can pick some promising products from outputs of groups:

  Product | Outputs of Groups   | Descriptions
----------------------| ---------------------- | ----------------------------- 
HU                    | All                    | Whole IVI product
V2X                   | All                    | Whole V2X product
Security Solution     | Architecutre, Security | Concept, Architecture, Components, and Test
SAL                   | SAL                    | Carplay, Carlife, Android Auto
Audio                 | Audio                  | Audio Implementation, Audio Tuning
OTA                   | OTA                    | OTA client, protocol, cloud setup and maintain
BSP/Framework/APP Dev | BSP/Framework/App      | Outsource BSP development
...                   | ...                    | ...

## Physical Containers
A suitable container, here is folder, is necessary to match the organizational structure and to accommodate the projects, documents, source code, and COC.

```c++
projects
   common
      common
      engineering
         common
         hardware
         mechanical
         pm
         process
         quality
         software
            common
            architecture
            development
               applications
               bsp
               framework
               audio
               ...
            integration
            requirements
            spm
            test
         sytem
         test
      hr
      finance
      it
      product
      production
      purchasing
      sales
   ivi
      common
      saicrx5
         // same as projects/common
         engineering
            software
               development // here is project source code and workspace
         // same as projects/common
      gacip31
   v2x
      common
      gwma55
      fordd31
      ...
   adas
      common
      geelyb33
      svwa21
      ...
   ...
```
To make folders in order and knowledge/skills/techniques/component shared, we use the *Abstraction Mechanism* from C++. 

* ***common*** is base forder (base class) which is a place to contain the common parts of the level in which the common is. Such as:
   * projects/common: the common parts for all products
   * projects/ivi/common: the common parts of ivi product
   * projects/v2x/common: the common parts of v2x product
   * projects/adas/common: the common parts of adas product
   * projects/ivi/common/engineering/software/common: the common parts of software
* ***prjfolder*** is project specific folder (concret class) which is a place to contain proprietary parts of project. Please note the project specific contents can be abstracted and moved into the *projects/xxx/common* folder or *projects/common* folder if they can be shared in specific product or in all products.
   * projects/ivi/saicrx5: the project specific folder for the project "*saicrx5*" of ivi product.
   * projects/v2x/gwma55: the project specific folder for the project "*gwma55*" of vx2 product.

With time going by, the shared knowledge, skills, techniques, components, process, and e.t.c. will be deposited in common folder, which can be shared and reused in kinds of projects and varieties of products. That is the essences of COC.

# Glossary
| Term | Definition                              |
| ---- | --------------------------------------- |
| IVI  | In-Vehicle Infortainment                |
| IPC  | Inter-Process Communication             |
| HU   | Head Unit                               |
| PM   | Project Manager                         |
| SPM  | Software Project Manager                |
| SAL  | Smart Application Link                  |
| OTA  | Over-the-air Update                     |
| COC  | Center of competence


# References
1.	http://www.melconway.com/Home/Committees_Paper.html
2.	http://www.melconway.com/Home/pdf/committees.pdf
3.	https://www.cnblogs.com/ghj1976/p/5703462.html
4.	https://en.wikipedia.org/wiki/Conway%27s_law
5.	https://blog.csdn.net/icqw1983/article/details/83782014
6.	https://medium.com/@emma_j_saunders/why-organization-design-shouldn-t-be-nice-8f34d0a54751
7.	https://www.heflo.com/blog/business-management/small-business-organizational-structure-examples/
8.	https://smallbusiness.chron.com/flat-vs-hierarchical-organizational-structure-724.html
9.	https://blog.csdn.net/samxx8/article/details/51930577
10.	https://www.infoq.cn/article/K9qVBKjfzh3lFzzJ_MvF
11.	https://rationalwiki.org/wiki/Discordianism#SNAFU_Principle
12.	https://www.infoq.cn/article/every-architect-should-study-conway-law
13.	https://www.cs.drexel.edu/~yfcai/CS451/RequiredReadings/MythicalManMonth.pdf
14. http://mamund.com/talks/2016-03-qconsp-teams/transcript.html
15. http://mamund.com/talks/2016-03-qconsp-teams/2016-03-qconsp-teams.pdf
16. https://medium.com/@Alibaba_Cloud conways-law-a-theoretical-basis-for-the-microservice-architecture-c666f7fcc66a
17. http://ressources.unisciel.fr/algoprog/s00aaroot/aa00module1/res/%5BCormen-AL2011%5DIntroduction_To_Algorithms-A3.pdf, Chapter 4
18. https://www.infoq.com/articles/standish-chaos-2015/
19. https://andrewbegel.com/papers/coordination-chase09.pdf
20. http://www.sohu.com/a/42073902_120865
21. https://backend.orbit.dtu.dk/ws/portalfiles/portal/126260733/Bekdik_and_Thuesen_EPOC_2016.pdf
22. https://www.microsoft.com/en-us/research/publication/the-influence-of-organizational-structure-on-software-quality-an-empirical-case-study
23. https://martinfowler.com/articles/microservices.html
24. https://herbsleb.org/web-pres/slides/Siemens-conference-7-17-08-dist.pdf
25. https://www.proofhub.com/articles/benefits-of-cross-functional-team-collaboration
26. https://www.scaledagileframework.com
27. TheIngredientsOfProduct.pdf

